# code-snippets
Well-written code and other code-snippets

## Spock

```groovy
new PollingConditions(timeout: 10, initialDelay: 0.5).eventually {
    MyProcess myProcess = getMyProcess(someResponse.json.data.id as String)
    assert myProcess.getStatus() == MyStatus.DONE
}
```

## Maven

```xml
<project>
    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <version>1.0.0</version>
                <executions>
                    <execution>
                        <phase>initialize</phase>
                        <goals>
                            <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                            <files>
                                <!-- without this the properties file is not available for the sonar-maven-plugin -->
                                <file>sonar-project.properties</file>
                            </files>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

## Spring/Java

```java
@Configuration
public class BeanConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return Jackson2ObjectMapperBuilder.json().build()
            .findAndRegisterModules()
            .registerModule(jacksonModuleFixingTimestampSerialization())
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .disable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
    }
    
    /**
     * Force Jackson to use 3 digits for the fractions of a second instead of the default 6
     * for Instant and ZonedDateTime objects.
     * See https://stackoverflow.com/questions/47502158/force-milliseconds-when-serializing-instant-to-iso8601-using-jackson
     */
    private static SimpleModule jacksonModuleFixingTimestampSerialization() {
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ZonedDateTime.class,
            new ZonedDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX")));
        simpleModule.addSerializer(Instant.class, new InstantSerializerWithMilliSecondPrecision());
        return simpleModule;
    }
}
```

```java
Instant.now().getEpochSecond()
```

```java
LocalDateTime.now(ZoneOffset.UTC)
```

Use Java Optional to retrieve and add, if present, a parameter.
```java
Map<String, Object> params = new HashMap<>();
myParameterObject.getMyParameter().ifPresent(v -> params.put("keyForMyParameter", v));
```

## Karate

```javascript
* json myVariableAsJson = response.message.payload
```

```javascript
* configure retry = { count: 20, interval: 1000 }
And retry until response.data.status == 'completed'
```

common.feature - [Multiple Functions in One File](https://intuit.github.io/karate/#multiple-functions-in-one-file)
```javascript
@ignore
Feature: Define common utility methods, constants and helpers.

    Scenario:
        * def myCommonValue = 500
        
        * def randomUUID =
            """
            function() {
                return java.util.UUID.randomUUID().toString();
            }
            """
```

secondfile.feature
```javascript
Feature: Here are the actual tests.

    Background:
        * call read('classpath:common.feature')
```

## Kubernetes

```yaml
hostname: my-service-local.127.0.0.1.nip.io
```
[nip.io](https://nip.io/)
The "dash" notation is especially useful when using services like LetsEncrypt as it's just a regular sub-domain of nip.io 

## Jenkinsfile

Remove used resources
```groovy
post {
  always {
    deleteDir()
  }
}
```

## SSH

Add the following to the `$HOME/.ssh/config` file to avoid logging in twice when using a proxy server for SSH access.

```config
Host web
  HostName my-tracking-server
  User me
  ProxyJump me@my-tracking-server.net
  IdentityFile ~/.ssh/id_rsa
```

## Cron

```config
# scheduled reboot Sunday at 04:30
# https://openwrt.org/docs/guide-user/base-system/cron#periodic_reboot
30 4 * * 0 sleep 70 && touch /etc/banner && reboot
```

## Expect

Source is here: [Login with GNU screen with username and password in a script](https://unix.stackexchange.com/questions/394891/login-with-gnu-screen-with-username-and-password-in-a-script)

```shell
#!/usr/bin/expect -f

set username "hello"
set password "secret"
spawn screen /dev/ttyUSB1 115200

expect "?ogin:"
send "$username\r"

expect "?assword:"
send "$password\r"

send "ls\r"

```


Repository icon made by [Dave Gandy](https://www.flaticon.com/authors/dave-gandy).
